<?php session_start();
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CS447</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <link rel="stylesheet" href="css/styles.css">
    </head>
    
    <body>  
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script></body>
        
          <nav class="navbar navbar-light">
            <div class="container-fluid">
              <a class="navbar-brand">ระบบแจ้งจบการศึกษา</a>
              <!-- <form class="d-flex">
                <p>ชื่อ-นามสกุล</p>
                <p></p>
              </form> -->
            </div>
          </nav>
          <?php
              //Data form aws
              define('DB_SERVER', 'myrds.cjorygmccf0z.us-east-1.rds.amazonaws.com');
              define('DB_USERNAME', 'admin');
              define('DB_PASSWORD', 'hey123456789');
              define('DB_DATABASE', 'MyData');

              /* Connect to MySQL and select the database. */
              $connection = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD);

              if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();

              $database = mysqli_select_db($connection, DB_DATABASE);
              if (!$database){
                die ("Database connection failed!");
              }
            ?>

          <div class="container">
            <table class="table table-light text-center">
              <p class="header-table">ผลการตรวจสอบข้อมูลก่อนทำการยืนยันจบการศึกษา</p>
              <hr />
              <thead>
                <tr>
                  <th scope="col">ลำดับที่</th>
                  <th scope="col">เงื่อนไขในการตรวจสอบ</th>
                  <th scope="col">ผลการตรวจสอบ</th>
                  <th scope="col">รายละเอียด</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <th scope="row">1</th>
                  <td>เรียนครบหลักสูตร</td>
                  <?php
                      $result = mysqli_query($connection, "SELECT * FROM Students where fname = '".$_SESSION['fname']."' AND lname = '".$_SESSION['lname']."'");
                    if (!$result){
                      die ("Database connection failed!");
                    }else{
                      $query_data = mysqli_fetch_row($result);
                        if($query_data[3]==1){
                          $_SESSION['x1'] = 1;
                          echo "<td>","ครบ", "</td>";
                        }else{
                          echo "<td>","ไม่ครบ", "</td>";
                        }
                    }
                  ?>
                  <td>รายละเอียด</td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>หน่วยกิตครบ</td>
                  <?php
                    $result = mysqli_query($connection, "SELECT * FROM Students where fname = '".$_SESSION['fname']."' AND lname = '".$_SESSION['lname']."'");
                    if (!$result){
                      die ("Database connection failed!");
                    }else{
                      $query_data = mysqli_fetch_row($result);
                        if($query_data[4]==1){ 
                          $_SESSION['x2'] = 1;
                          echo "<td>","ครบ", "</td>";
                         
                        }else{
                          echo "<td>","ไม่ครบ", "</td>";
                        }
                    }
                  ?>
                  <td>รายละเอียด</td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>เกรดรวม</td>
                  <?php
                    $result = mysqli_query($connection, "SELECT * FROM Students where fname = '".$_SESSION['fname']."' AND lname = '".$_SESSION['lname']."'");
                    if (!$result){
                      die ("Database connection failed!");
                    }else{
                      $query_data = mysqli_fetch_row($result);
                        if($query_data[5]==1){
                          $_SESSION['x3'] = 1;
                          echo "<td>","ครบ", "</td>";
                          
                        }else{
                          echo "<td>","ไม่ครบ", "</td>";
                        }
                    }
                  ?>
                  <td>รายละเอียด</td>
                </tr>
                <tr>
                  <th scope="row">4</th>
                  <td>เกรดรายวิชาในสาขา</td>
                  <?php
                    $result = mysqli_query($connection, "SELECT * FROM Students where fname = '".$_SESSION['fname']."' AND lname = '".$_SESSION['lname']."'");
                    if (!$result){
                      die ("Database connection failed!");
                    }else{
                      $query_data = mysqli_fetch_row($result);
                        if($query_data[7]==1){
                          $_SESSION['x4'] = 1;
                          echo "<td>","ครบ", "</td>";
                        }else{
                          echo "<td>","ไม่ครบ", "</td>";
                        }
                    }
                  ?>
                  <td>รายละเอียด</td>
                </tr>
              </tbody>
            </table>

            <div id="info_div" style="display:none">
              <p>ข้อมูลนักศึกษา</p>
              <hr />
              <div class="row">
                <div class="col">
                  <?php  $result = mysqli_query($connection, "SELECT * FROM Students where fname = '".$_SESSION['fname']."' AND lname = '".$_SESSION['lname']."'");
                          $query_data = mysqli_fetch_row($result);
                          echo "<h6>","เลขประจำตัวนักศึกษา : {$query_data[0]}","</h6>"; ?>
                   <?php echo "<h6>","ชื่อ-นามสกุล : {$_SESSION['fname']}  {$_SESSION['lname']}", "</h6>"; ?> </h6>
                  <h6>ระดับการศึกษา : ปริญญาตรี</h6>
                  <h6>คณะ/สาขา : วิทยาศาสตร์</h6>
                </div>
               <form action="S3_index_form.php" method="post" enctype="multipart/form-data" > 
               <div class="col-6">
                  <img src="" />
                  <input class="form-control form-control-sm" name="fileToUpload" id="formFileSm" type="file"  />
                </div>
              </div>
                <div class="d-grid gap-2 d-md-flex justify-content-md-end" >
                  <button type="submit"  name="submit" class="btn btn-primary">ยื่นขอจบการศึกษา</button>
                </div>
              </form>
            </div>
          </div>
          <!-- Update link_pic -->
          <?php
            if($_SESSION['x1'] == 1 && $_SESSION['x2'] == 1 && $_SESSION['x3'] == 1 && $_SESSION['x4'] == 1){
              echo "<script>
              document.getElementById('info_div').style.display = 'block';
              </script>";
            }
          ?>
    </body>
</html>     
